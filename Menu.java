import java.util.Scanner;
public class Exercicio01 {
    public static void main(String[] args){
        System.out.println("|       Menu         |");
        System.out.println("|    Opções:         |");
        System.out.println("|         Opção 1    |");
        System.out.println("|         Opção 2    |");
        System.out.println("|         Opção 3    |");
        Scanner menu = new Scanner(System.in);
        boolean opcao_sair = false;
        do{        
            System.out.println("Selecione uma opção: ");
            int opcao = menu.nextInt();
            
            if (opcao != 3){
                switch (opcao){
                    case 1:
                        System.out.println("Opção 1 Selecionada!");
                        break;
                    case 2:
                        System.out.println("Opção 2 Selecionada!");
                        break;
                    case 3:
                        System.out.println("Sair");
                        break;
                    default:
                        System.out.println("Opção inválida!");
                        break;
                }
                opcao_sair = true;
            }
            else{
                opcao_sair = false;
            }
        }while(opcao_sair);
    }
}
